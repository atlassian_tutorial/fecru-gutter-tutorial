package com.atlassian.tutorials.fecru.gutter;

import com.atlassian.fisheye.ui.filedecoration.DiffLineDecorator;
import com.atlassian.fisheye.ui.filedecoration.DiffType;
import com.atlassian.fisheye.ui.filedecoration.GutterRenderer;
import com.atlassian.fisheye.ui.filedecoration.LineDecorator;

import java.util.Collections;
import java.util.List;

public class TutorialGutterRenderer implements GutterRenderer {

    public List<LineDecorator> getAnnotationDecorators(String repository, String path, String rev) {
        String fileName = path.substring(path.lastIndexOf('/')+1);
        if (fileName.matches(".*[aA].*")) {
            return Collections.<LineDecorator>singletonList(new TutorialLineDecorator(fileName.length()));
        } else {
            return Collections.emptyList();            
        }
    }

    public List<DiffLineDecorator> getAnnotationDecorators(DiffType diffType, String repository, String fromPath, String fromRev, String toPath, String toRev) {
        return Collections.emptyList();
    }
}
