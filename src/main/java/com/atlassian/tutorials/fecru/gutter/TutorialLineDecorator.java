package com.atlassian.tutorials.fecru.gutter;

import com.atlassian.fisheye.ui.filedecoration.GutterRenderResult;
import com.atlassian.fisheye.ui.filedecoration.LineDecorator;

import java.util.List;


public class TutorialLineDecorator implements LineDecorator {
    private final int lineToDecorate;

    public TutorialLineDecorator(int lineToDecorate) {
        this.lineToDecorate = lineToDecorate;
    }

    public GutterRenderResult decorateLine(int i) {
        GutterRenderResult r = new GutterRenderResult();
        if (i == lineToDecorate) {
            r.setInnerHtml("Decoration!");            
        }
        return r;
    }
}
